﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CookComputing.XmlRpc;
using System.Windows.Forms;
using System.Dynamic;
using odooOutlookSettings;
using OdooOutlookAddIn;

namespace odooWebservice
{
    public interface ICommon : IXmlRpcProxy
    {
        [XmlRpcMethod]
        int authenticate(string db, string user, string pass, object arguments);
    }

    public interface IObject : IXmlRpcProxy
    {
        [XmlRpcMethod]
        object execute_kw(string db, int uid, string password, string modelName, string methodName,
            object[] argsByPosition, object argsByKeyword);
    }

    public class loginFailedException : System.Exception { }

    public class odooWebserviceConnector
    {
        public int uid;
        public int partnerID;

        private ICommon common;
        private IObject models;


        private int getPartnerID(int userID)
        {
            object[] argsList = new object[] { new object[] { new object[] { "id", "=", userID } } };
            XmlRpcStruct argsDict = new XmlRpcStruct();
            argsDict.Add("fields", new object[] { "partner_id" });
            object[] userData = (object[])this.execute_kw("res.users", "search_read", argsList, argsDict);

            dynamic user = (XmlRpcStruct)userData[0];

            return (int)(user["partner_id"][0]);
        }

        public odooWebserviceConnector()
        {
            Settings settings = new Settings();

            string url = settings.server;
            string https_url = url;
            string db = settings.db;
            string user = settings.user;
            string pass = settings.pass;

            common = XmlRpcProxyGen.Create<ICommon>();
            models = XmlRpcProxyGen.Create<IObject>();

            if (url.Length < 4 || url.Substring(0, 4) != "http")
            {
                https_url = "https://" + url;
                url = "http://" + url;
            }

            common.Url = url + "/xmlrpc/2/common";
            models.Url = url + "/xmlrpc/2/object";

            // automatically log the user in
            try
            {
                this.uid = common.authenticate(db, user, pass, new object());
            }
            catch (Exception e)
            {
                if (url != https_url)
                {
                    common.Url = https_url + "/xmlrpc/2/common";
                    models.Url = https_url + "/xmlrpc/2/object";

                    try
                    {
                        this.uid = common.authenticate(db, user, pass, new object());
                    }
                    catch (Exception)
                    {
                        // Login failed for whatever reason. log it tho
                        Logger.Log(e.Message, e.StackTrace);
                        throw new loginFailedException();
                    }
                }
                else
                {
                    // Login failed for whatever reason. log it tho
                    Logger.Log(e.Message, e.StackTrace);
                    throw new loginFailedException();
                }
            }

            this.partnerID = this.getPartnerID(this.uid);
        }

        public odooWebserviceConnector(string url, string db, string user, string pass)
        {
            common = XmlRpcProxyGen.Create<ICommon>();
            models = XmlRpcProxyGen.Create<IObject>();
            string https_url = url;

            if (url.Length < 4 || url.Substring(0, 4) != "http")
            {
                https_url = "https://" + url;
                url = "http://" + url;
            }

            common.Url = url + "/xmlrpc/2/common";
            models.Url = url + "/xmlrpc/2/object";

            // automatically log the user in
            try
            {
                this.uid = common.authenticate(db, user, pass, new object());
            }
            catch (Exception e)
            {
                if (url != https_url)
                {
                    common.Url = https_url + "/xmlrpc/2/common";
                    models.Url = https_url + "/xmlrpc/2/object";

                    try
                    {
                        this.uid = common.authenticate(db, user, pass, new object());
                    }
                    catch (Exception)
                    {
                        // Login failed for whatever reason. log it tho
                        Logger.Log(e.Message, e.StackTrace);
                        throw new loginFailedException();
                    }
                }
                else
                {
                    // Login failed for whatever reason. log it tho
                    Logger.Log(e.Message, e.StackTrace);
                    throw new loginFailedException();
                }
            }

            this.partnerID = this.getPartnerID(this.uid);
        }

        public object execute_kw(string model, string method, object[] argsList = null, object argsDict = null)
        {
            odooOutlookSettings.Settings settings = new odooOutlookSettings.Settings();

            if (argsList == null)
            {
                argsList = new object[0];
            }
            if (argsDict == null)
            {
                argsDict = new object();
            }
            return this.models.execute_kw(settings.db, this.uid, settings.pass, model, method, argsList, argsDict);
        }
    }
}
