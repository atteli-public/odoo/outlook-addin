﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using odooWebservice;
using odooOutlookSync;
using odooOutlookSettings;

namespace OdooOutlookAddIn
{
    public partial class connect : Form
    {
        private Addin parentWindow;

        public connect(Addin parentWindow)
        {
            this.parentWindow = parentWindow;
            InitializeComponent();

            Settings settings = new Settings();
            this.serverText.Text = settings.server;
            this.databaseText.Text = settings.db;
            this.usernameText.Text = settings.user;
            this.passwordText.Text = settings.pass;

            if(settings.syncPriority == "Outlook")
            {
                this.outlookPrio.Checked = true;
            }
            else
            {
                this.odooPrio.Checked = true;
            }

            if (settings.syncFreq == "1h")
            {
                this.sync1h.Checked = true;
            }
            else if (settings.syncFreq == "15m")
            {
                this.sync15Mins.Checked = true;
            }
            else
            {
                this.syncManual.Checked = true;
            }

            this.autoUpdate.Checked = settings.autoUpdate;
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            string server = this.serverText.Text;
            string database = this.databaseText.Text;
            string username = this.usernameText.Text;
            string password = this.passwordText.Text;

            Settings settings = new Settings();
            settings.server = server;
            settings.db = database;
            settings.user = username;
            settings.pass = password;

            if (this.outlookPrio.Checked)
            {
                settings.syncPriority = "Outlook";
            }
            else
            {
                settings.syncPriority = "Odoo";
            }

            if (this.sync1h.Checked)
            {
                settings.syncFreq = "1h";
                this.parentWindow.syncThread.Abort();
                this.parentWindow.syncThread = new Thread(new ThreadStart(this.parentWindow.cron_sync));
                this.parentWindow.syncThread.Start();
            }
            else if (this.sync15Mins.Checked)
            {
                settings.syncFreq = "15m";
                this.parentWindow.syncThread.Abort();
                this.parentWindow.syncThread = new Thread(new ThreadStart(this.parentWindow.cron_sync));
                this.parentWindow.syncThread.Start();
            }
            else
            {
                settings.syncFreq = "Man";
                this.parentWindow.syncThread.Abort();
            }

            if (this.autoUpdate.Checked)
            {
                this.parentWindow.setTaskScheduler();
            }
            else
            {
                this.parentWindow.disableTaskScheduler();
            }

            try
            {
                new odooWebserviceConnector(server, database, username, password);
            }
            catch (loginFailedException)
            {
                MessageBox.Show("Login failed, please try again.");
                return;
            }

            MessageBox.Show("Login successful!");
            this.Close();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
