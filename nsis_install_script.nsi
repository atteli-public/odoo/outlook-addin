Name "Outlook Add-In Installer"
OutFile "OutlookAddInInstaller.exe"

RequestExecutionLevel highest

!include 'LogicLib.nsh'
!include "Registry.nsh"
!include "x64.nsh"
!include "MUI.nsh"

; string replace

!define StrRep "!insertmacro StrRep"
!macro StrRep output string old new
    Push `${string}`
    Push `${old}`
    Push `${new}`
    !ifdef __UNINSTALL__
        Call un.StrRep
    !else
        Call StrRep
    !endif
    Pop ${output}
!macroend
 
!macro Func_StrRep un
    Function ${un}StrRep
        Exch $R2 ;new
        Exch 1
        Exch $R1 ;old
        Exch 2
        Exch $R0 ;string
        Push $R3
        Push $R4
        Push $R5
        Push $R6
        Push $R7
        Push $R8
        Push $R9
 
        StrCpy $R3 0
        StrLen $R4 $R1
        StrLen $R6 $R0
        StrLen $R9 $R2
        loop:
            StrCpy $R5 $R0 $R4 $R3
            StrCmp $R5 $R1 found
            StrCmp $R3 $R6 done
            IntOp $R3 $R3 + 1 ;move offset by 1 to check the next character
            Goto loop
        found:
            StrCpy $R5 $R0 $R3
            IntOp $R8 $R3 + $R4
            StrCpy $R7 $R0 "" $R8
            StrCpy $R0 $R5$R2$R7
            StrLen $R6 $R0
            IntOp $R3 $R3 + $R9 ;move offset by length of the replacement string
            Goto loop
        done:
 
        Pop $R9
        Pop $R8
        Pop $R7
        Pop $R6
        Pop $R5
        Pop $R4
        Pop $R3
        Push $R0
        Push $R1
        Pop $R0
        Pop $R1
        Pop $R0
        Pop $R2
        Exch $R1
    FunctionEnd
!macroend
!insertmacro Func_StrRep ""
!insertmacro Func_StrRep "un."

; end string replace



; Linear Congruential Generator
; Park, S.K. and K.W. Miller, 1988; Random Number Generators: Good Ones are Hard to Find,
; Comm. of the ACM, V. 31. No. 10, pp 1192-1201
; n(k+1) = (a * n(k) + b) mod m
; m=2147483647, a=16807, b=0 
; the seed after 10000 iterations should be 1043618065 for n(0)=1
; http://www.mactech.com/articles/mactech/Vol.08/08.03/RandomNumbers/
; 
; Syntax:
; ${Random} "Seed" "Min" "Max
; 
; "Seed"     ; Seed value for generator or last generated random number
; "Min"      ; name of the initialization file
; "Max"      ; Callback function then found
; 
; Stack 1    ; Random number between 0x00000000 and 0X7FFFFFFF [0x00000000;0X7FFFFFFF]
; Stack 2    ; Random number between min and max [MIN;MAX]
; 
; e.g.
; Do:
;     IntOp $0 $0 + 1
;     ${Random} $1 0 255
;     Pop $1
;     Pop $2
;     StrCmp $0 "10000" 0 Do
; StrCmp $1 1043618065 0 +2
;     MessageBox MB_OK "Test Minimal Standard Generator o.k."
;
!define Random `!insertmacro RandomCall`
 
!macro RandomCall _SEED _MIN _MAX
	; save global registers
	Push $0
	Push $1
	Push $2
	Push $3
	Push $4
	Push $5
	Push $6
	Push $7
	Push $8
	Push $9		
	; store parameter
	Push `${_SEED}`
	Push `${_MIN}`
	Push `${_MAX}`
	; execute function
Call Random
	; restore global register
	Pop $9
	Pop $8
	Pop $7
	Pop $6
	Pop $5
	Pop $4
	Pop $3
	Pop $2
	
	Exch $1
	Exch
	Exch $0
!macroend
Function Random
    Pop $2 ; _MAX
    Pop $1 ; _MIN
    Pop $0 ; _SEED
 
    ; n(k+1) = (a * n(k) + b) mod m
    System::Int64Op /NOUNLOAD 16807 * $0
    Pop $0
    System::Int64Op /NOUNLOAD $0 + 0
    Pop $0
    System::Int64Op /NOUNLOAD $0 % 0X7FFFFFFF
    Pop $0
 
    ; calculate value betwenn _MIN _MAX
    IntOp $3 $2 - $1
    IntOp $3 $3 + 1
    System::Int64Op /NOUNLOAD $0 * $3
    Pop $4
    System::Int64Op /NOUNLOAD $4 / 0X7FFFFFFF
    Pop $4
    IntOp $1 $4 + $1
 
FunctionEnd

ReserveFile "OutlookAddIn\bin\Debug\OdooOutlookAddIn.dll"
ReserveFile "OutlookAddIn\bin\Debug\OdooOutlookAddIn.pdb"
ReserveFile "OutlookAddIn\bin\Debug\CookComputing.XmlRpcV2.dll"
ReserveFile "OutlookAddIn\bin\Debug\NetOffice.dll"
ReserveFile "OutlookAddIn\bin\Debug\OfficeApi.dll"
ReserveFile "OutlookAddIn\bin\Debug\OutlookApi.dll"
ReserveFile "OutlookAddIn\bin\Debug\Newtonsoft.Json.dll"
ReserveFile "OutlookAddIn\bin\Debug\Microsoft.Win32.TaskScheduler.dll"

InstallDir "$APPDATA\OdooOutlookAddIn"

!define MUI_PAGE_HEADER_TEXT "Directory"
!insertmacro MUI_PAGE_DIRECTORY
!define MUI_PAGE_HEADER_TEXT "Installing"
!insertmacro MUI_PAGE_INSTFILES
!define MUI_PAGE_HEADER_TEXT ""
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES


Section "Outlook Add-In"
	${registry::KeyExists} "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Outlook\Addins" $R0
	
	${If} $R0 == -1
		MessageBox MB_OK "Microsoft Outlook is not installed."
		Quit
	${EndIf}
	
	${If} ${FileExists} `$INSTDIR`
	  ; file is a directory
	${ElseIf} ${FileExists} `$INSTDIR`
	  ; file is a file
		MessageBox MB_OK "Please specify a folder not a file."
		Quit
	${Else}
	  ; file is neither a file or a directory (i.e. it doesn't exist)
	  CreateDirectory $INSTDIR
	${EndIf}

	SetOutPath $INSTDIR
	File "OutlookAddIn\bin\Debug\OdooOutlookAddIn.dll"
	File "OutlookAddIn\bin\Debug\OdooOutlookAddIn.pdb"
	File "OutlookAddIn\bin\Debug\CookComputing.XmlRpcV2.dll"
	File "OutlookAddIn\bin\Debug\NetOffice.dll"
	File "OutlookAddIn\bin\Debug\OfficeApi.dll"
	File "OutlookAddIn\bin\Debug\OutlookApi.dll"
	File "OutlookAddIn\bin\Debug\Newtonsoft.Json.dll"
	File "OutlookAddIn\bin\Debug\Microsoft.Win32.TaskScheduler.dll"
	${StrRep} "$R1" "$INSTDIR" "\" "/"
	
	; register add-in in outlook registry
	
	${registry::CreateKey} "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Outlook\Addins\OdooOutlookAddIn.Addin" $R0
	${registry::Write} "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Outlook\Addins\OdooOutlookAddIn.Addin" "Description" "OdooOutlookAddIn" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Outlook\Addins\OdooOutlookAddIn.Addin" "FriendlyName" "OdooOutlookAddIn" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Outlook\Addins\OdooOutlookAddIn.Addin" "LoadBehavior" 3 "REG_DWORD" $R0
	${registry::Write} "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Outlook\Addins\OdooOutlookAddIn.Addin" "TempFolder" "$INSTDIR\Temp" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Outlook\Addins\OdooOutlookAddIn.Addin" "InstallFolder" "$INSTDIR" "REG_SZ" $R0
	CreateDirectory $INSTDIR\Temp
	
	; create settings folder
	${registry::CreateKey} "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Outlook\Addins\OdooOutlookAddIn.Addin\Settings" $R0
	${registry::Write} "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Office\Outlook\Addins\OdooOutlookAddIn.Addin\Settings" "VersionNumber" "0.9" "REG_SZ" $R0
	
	; our unique ID is 5951b016-e77c-4652-b421-a082a60210ce
	
	; register COM class
	${registry::CreateKey} "HKEY_CURRENT_USER\Software\Classes\OdooOutlookAddIn.Addin" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\OdooOutlookAddIn.Addin" "" "OdooOutlookAddIn.Addin" "REG_SZ" $R0
	${registry::CreateKey} "HKEY_CURRENT_USER\Software\Classes\OdooOutlookAddIn.Addin\CLSID" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\OdooOutlookAddIn.Addin\CLSID" "" "{5951b016-e77c-4652-b421-a082a60210ce}" "REG_SZ" $R0
	
	; register 64-bit COM class
	SetRegView 64
	${registry::CreateKey} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}" "" "OdooOutlookAddIn.Addin" "REG_SZ" $R0
	
	${registry::CreateKey} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" "" "mscoree.dll" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" "Assembly" "OdooOutlookAddIn, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" "Class" "OdooOutlookAddIn.Addin" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" "CodeBase" "file:///$R1/OdooOutlookAddIn.dll" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" "RuntimeVersion" "v4.0.30319" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" "ThreadingModel" "Both" "REG_SZ" $R0
	
	${registry::CreateKey} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\ProgId" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\ProgId" "" "OdooOutlookAddIn.Addin" "REG_SZ" $R0
	
	${registry::CreateKey} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32\1.0.0.0" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32\1.0.0.0" "Assembly" "OdooOutlookAddIn, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32\1.0.0.0" "Class" "OdooOutlookAddIn.Addin" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32\1.0.0.0" "CodeBase" "file:///$R1/OdooOutlookAddIn.dll" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32\1.0.0.0" "RuntimeVersion" "v4.0.30319" "REG_SZ" $R0
	
	; register 32-bit COM class
	SetRegView 32
	${registry::CreateKey} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}" "" "OdooOutlookAddIn.Addin" "REG_SZ" $R0
	
	${registry::CreateKey} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" "" "mscoree.dll" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" "Assembly" "OdooOutlookAddIn, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" "Class" "OdooOutlookAddIn.Addin" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" "CodeBase" "file:///$R1/OdooOutlookAddIn.dll" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" "RuntimeVersion" "v4.0.30319" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32" "ThreadingModel" "Both" "REG_SZ" $R0
	
	${registry::CreateKey} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\ProgId" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\ProgId" "" "OdooOutlookAddIn.Addin" "REG_SZ" $R0
	
	${registry::CreateKey} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32\1.0.0.0" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32\1.0.0.0" "Assembly" "OdooOutlookAddIn, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32\1.0.0.0" "Class" "OdooOutlookAddIn.Addin" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32\1.0.0.0" "CodeBase" "file:///$R1/OdooOutlookAddIn.dll" "REG_SZ" $R0
	${registry::Write} "HKEY_CURRENT_USER\Software\Classes\Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}\InprocServer32\1.0.0.0" "RuntimeVersion" "v4.0.30319" "REG_SZ" $R0
	
	; --------------------
	; | create salt file |
	; --------------------
	
	; get time as seconds from unix epoch to use as the initial seed
	system::call *(&i16,l)i.s
	system::call 'kernel32::GetLocalTime(isr0)'
	IntOp $1 $0 + 16
	system::call 'kernel32::SystemTimeToFileTime(ir0,ir1)'
	system::call *$1(l.r1)
	system::free $0
	system::Int64Op $1 / 10000000
	Pop $1
	system::Int64Op $1 - 11644473600
	Pop $1
	StrCpy $R1 0
	; go through 1000 iterations of random function
	${While} $R1 < 1000
		${Random} $1 0 255
		Pop $1
		Pop $2
		IntOp $R1 $R1 + 1
	${EndWhile}
	
	FileOpen $9 "$INSTDIR\salt" w
	StrCpy $R1 0
	; generate random byte sequence with length 64
	${While} $R1 < 64
		${Random} $1 0 255
		Pop $1
		Pop $2
		FileWriteByte $9 $2
		IntOp $R1 $R1 + 1
	${EndWhile}
	
	WriteUninstaller "$INSTDIR\uninstall.exe"
SectionEnd

 
Function un.onInit
	SetShellVarContext all
 
	#Verify the uninstaller - last chance to back out
	MessageBox MB_OKCANCEL "Permanantly remove Odoo Outlook Add-In?" IDOK next
		Abort
	next:
FunctionEnd
 
Section "uninstall"
 
	# Remove files
	Delete $INSTDIR\OdooOutlookAddIn.dll
	Delete $INSTDIR\OdooOutlookAddIn.pdb
	Delete $INSTDIR\CookComputing.XmlRpcV2.dll
	Delete $INSTDIR\NetOffice.dll
	Delete $INSTDIR\OfficeApi.dll
	Delete $INSTDIR\OutlookApi.dll
	Delete $INSTDIR\Newtonsoft.Json.dll
	Delete $INSTDIR\salt
	Delete $INSTDIR\Microsoft.Win32.TaskScheduler.dll
	Delete $INSTDIR\log.txt
 
	# Always delete uninstaller as the last action
	Delete $INSTDIR\uninstall.exe
 
	# Try to remove the install directory - this will only happen if it is empty
	rmDir $INSTDIR\Temp
	rmDir $INSTDIR
 
	# Remove uninstaller information from the registry
	DeleteRegKey HKCU "SOFTWARE\Microsoft\Office\Outlook\Addins\OdooOutlookAddIn.Addin"
	DeleteRegKey HKCR "OdooOutlookAddIn.Addin"
	DeleteRegKey HKCR "CLSID\{5951b016-e77c-4652-b421-a082a60210ce}"
	DeleteRegKey HKCR "Wow6432Node\CLSID\{5951b016-e77c-4652-b421-a082a60210ce}"
SectionEnd