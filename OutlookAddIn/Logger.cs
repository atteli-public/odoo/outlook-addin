﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace OdooOutlookAddIn
{
    class Logger
    {
        public static object logLock = new Object();

        public static void LogShowMsgBox(string errorMsg, params string[] msgs)
        {
            Log(msgs);
            MessageBox.Show(errorMsg);
        }

        public static void Log(params string[] msgs)
        {
            try
            {
                var settings = new odooOutlookSettings.Settings();
                string installPath = settings.installPath;
                string all_msgs = "";

                foreach (string msg in msgs) {
                    all_msgs += msg + "\n";
                }

                lock (Logger.logLock)
                {
                    using (StreamWriter f = File.AppendText(installPath + "\\log.txt"))
                    {
                        f.WriteLine("\r\nLog Entry : {0} {1}\n\n{2}\n-------------------------------",
                            DateTime.Now.ToLongTimeString(),
                            DateTime.Now.ToLongDateString(),
                            all_msgs);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("An error occured while attempting to save the error report. Please install this Addin-In in a location to which your user has write access. You may also want to e-mail Atteli the following text:\n" + e.Message + e.StackTrace);
            }
        }
    }
}
